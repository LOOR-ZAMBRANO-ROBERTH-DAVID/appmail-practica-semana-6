//Importamos librerias
// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors, annotate_overrides

import 'package:flutter/material.dart';
import 'package:temmail/model/backend.dart';
import 'package:temmail/model/email.dart';

import 'detail_screen.dart';

class ListScreen extends StatefulWidget {
  @override
  State<ListScreen> createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  @override

  //Se crea una lista que contiene los emails------------
  List<Email> emails = Backend().getEmails();
//--------------------------------------------------
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Center(
              child: Text(
            "APPMAIL",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),
          )),
        ),
        body: ListView.builder(
            itemCount: emails.length,
            itemBuilder: (BuildContext context, int index) {
              final email = emails[index];
              final id = email.id;
              final bool read = email.read;
              return Dismissible(
                key: ValueKey(id),
                direction: DismissDirection.endToStart,
                background: Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      Text(
                        "Eliminar",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        "Correo",
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                child: Container(
                  child: Column(
                    children: [
                      ListTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              email.dateTime.toString(),
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 15),
                            ),
                            Text(
                              email.from,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 13),
                            ),
                          ],
                        ),
                        subtitle: Container(
                          padding: const EdgeInsets.only(top: 5.0),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(email.subject,
                                    style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.black,
                                    )),
                                Icon(
                                  read
                                      ? Icons.visibility_rounded
                                      : Icons.visibility_off,
                                  color: Color.fromARGB(255, 95, 102, 230),
                                ),
                              ]),
                        ),
                        onLongPress: () {
                          Backend().markEmailAsRead(id);
                          setState(() {});
                        },
                        onTap: () {
                          Backend().markEmailAsRead(id);
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => DetailScreen(
                                    email: emails[index],
                                  )));
                          setState(() {});
                        },
                      ),
                      Divider(color: Colors.lightBlue),
                    ],
                  ),
                ),
                onDismissed: (direction) {
                  setState(() {
                    emails.removeAt(index);
                    Backend().deleteEmail(id);
                    print(toString());
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        duration: Duration(seconds: 1), content: Text("$id")));
                    setState(() {});
                  });
                },
              );
            }));
  }
}
