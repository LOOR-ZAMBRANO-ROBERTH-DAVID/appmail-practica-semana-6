// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

import '../model/email.dart';

class DetailScreen extends StatelessWidget {
  Email email;
  DetailScreen({Key? key, required this.email}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(email.subject,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30)),
        backgroundColor: Colors.lightBlue,
        centerTitle: true,
      ),
      body: Column(children: [
        Row(mainAxisAlignment: MainAxisAlignment.start, children: [
          Padding(
            padding: EdgeInsets.only(top: 15, left: 10),
            child: Text(
              "From: ${email.from}",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
          ),
        ]),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Padding(
              padding: EdgeInsets.only(left: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(email.subject),
                  Padding(
                    padding: const EdgeInsets.only(left: 90),
                    child: Text(email.dateTime.toString(),
                        style: TextStyle(fontSize: 15)),
                  )
                ],
              ),
            ),
          ],
        ),
        Divider(color: Color.fromRGBO(72, 117, 206, 1)),
        Padding(
          padding: EdgeInsets.only(top: 10, left: 10, right: 10),
          child: Row(
            children: [
              Flexible(
                flex: 1,
                child: Text(email.body, style: TextStyle(fontSize: 20)),
              ),
            ],
          ),
        )
      ]),
    );
  }
}
