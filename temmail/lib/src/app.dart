import 'package:flutter/material.dart';
import 'package:temmail/screens/list_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.lightBlue,
      ),
      debugShowCheckedModeBanner: false,
      home: ListScreen(),
    );
  }
}
